import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

export class Card extends Component {
  render() {
    return (
      <View
        style={{
          backgroundColor: '#222831',
          width: '100%',
          flexDirection: 'row',
          marginVertical: 8,
        }}>
        <TouchableOpacity>
          <Image
            source={{uri: this.props.Poster}}
            style={{width: 100, height: 130, borderRadius: 10}}
          />
        </TouchableOpacity>
        <View style={{marginLeft: 15}}>
          <View
            style={{
              flexDirection: 'row',
              paddingTop: 10,
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 15, fontWeight: 'bold', color: '#EEEEEE'}}>
              {this.props.Title}
            </Text>
            <TouchableOpacity>
              <Icon
                name="download"
                size={16}
                style={{color: '#EEEEEE', marginRight: 'auto'}}
              />
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row', marginTop: 15}}>
            <Text style={{color: '#EEEEEE'}}>{this.props.Type}</Text>
            <Text style={{color: '#EEEEEE', marginLeft: 10}}>
              {this.props.Year}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default Card;
