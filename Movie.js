import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Card from './Card';

export class Movie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataAPI: [],
      isLoading: false,
      searchValue: 'avenger',
    };
  }

  componentDidUpdate() {
    this.handleChange;
    this.disalowNull();
  }

  componentDidMount() {
    this.fetchMovie();
  }

  fetchMovie = () => {
    fetch(`http://www.omdbapi.com/?s=avengers&apikey=997061b4&`)
      .then(response => response.json())
      .then(data => this.setState({dataAPI: data.Search}));
  };

  disalowNull = () => {
    if (this.state.searchValue == '') {
      this.setState({searchValue: 'avengers'});
    }
  };

  async handleChange() {
    try {
      const res = await fetch(
        `http://www.omdbapi.com/?s=${this.state.searchValue}&apikey=997061b4&`,
      );
      const data = await res.json();
      this.setState({dataAPI: data.Search});
    } catch (error) {
      this.fetchMovie();
    }
  }

  render() {
    // const { dataAPI } = this.state
    // console.log(this.state.dataAPI);
    return (
      <ScrollView
        style={{
          height: '100%',
          backgroundColor: '#222831',
          paddingHorizontal: '5%',
          paddingTop: 20,
        }}>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            alignItems: 'center',
            marginBottom: 15,
          }}>
          <Icon
            name="left"
            size={22}
            style={{color: '#EEEEEE', marginRight: 15}}
          />
          <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
            <Text style={{color: '#EEEEEE', fontSize: 18, fontWeight: 'bold'}}>
              Result
            </Text>
            <Text
              style={{
                color: '#EEEEEE',
                fontSize: 13,
                fontWeight: 'bold',
                marginLeft: 20,
              }}>
              {(this.state.dataAPI && this.state.dataAPI.length) || '0'} records
              available
            </Text>
          </View>
        </View>
        <View
          style={{
            alignItems: 'center',
            flexDirection: 'row',
            backgroundColor: '#393E46',
            width: '100%',
            height: 50,
            borderRadius: 10,
          }}>
          <Icon
            name="search1"
            size={16}
            style={{color: '#EEEEEE', marginLeft: 10}}
          />
          <TextInput
            placeholder="Search movies"
            placeholderTextColor="#EEEEEE"
            style={{width: '100%', marginLeft: 10, color: 'white'}}
            onChangeText={async e => {
              await this.setState({searchValue: e});
              await this.handleChange();
              await this.disalowNull();
            }}
          />
        </View>

        {this.state.dataAPI ? (
          this.state.dataAPI.map((e, i) => {
            return (
              <Card
                key={i}
                Poster={e.Poster}
                Title={e.Title}
                Type={e.Type}
                Year={e.Year}
              />
            );
          })
        ) : (
          <View
            style={{
              width: '100%',
              height: 200,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Icon
              name="find"
              size={22}
              style={{color: '#EEEEEE', marginRight: 15}}
            />
            <Text
              style={{
                color: 'white',
                marginTop: 20,
              }}>
              berfikir terlalu keras..
            </Text>
            <Text
              style={{
                color: 'white',
                marginTop: 10,
              }}>
              membuat kita melupakan
            </Text>
            <Text
              style={{
                color: 'white',
                marginTop: 10,
              }}>
              kesenangan yang sebenarnya..
            </Text>
          </View>
        )}
      </ScrollView>
    );
  }
}

export default Movie;
